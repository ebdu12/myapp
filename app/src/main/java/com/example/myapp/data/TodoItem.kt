package com.example.myapp.data

import java.text.SimpleDateFormat
import java.util.Date

data class TodoItem(
    val title :String,
    val date: Date
){
    fun getParsedDate(): String {
        return SimpleDateFormat("dd-MM-yyyy HH:mm").format(date)
    }
}
